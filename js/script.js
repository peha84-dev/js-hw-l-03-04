var headerB = "<header>";
var headerE = "</header>";
var mainB = "<main>";
var mainE = "</main>";
var footerB = "<footer>";
var footerE = "</footer>";

document.write(`${headerB} Домашні роботи по лекціям 3-4 ${headerE}`);

document.writeln(mainB);

/*
*    ДЗ 1
*/

// Використовуючи цикли намалюйте:
// 1 Порожній прямокутник
document.writeln("<div>");
let rectHeight = 7;
let rectWidth = 16;
for(let i = 1; i <= rectHeight; i++){
    for(let j = 1; j <= rectWidth; j++){
        if ((j === 1) || (j === rectWidth) || (i === 1) || (i === rectHeight))  {
            document.write("*");
        } else {
            document.write("&nbsp;");
        }
    }
    document.writeln("<br>");
}
document.writeln("</div>");

// 2 Заповнений рівносторонній трикутник - на рахунок рівностороннього не впевнений, але точно рівнобедрений
document.writeln("<div>");
let triangleWidth = 13;
let currentRowWidth = 1;
for (;currentRowWidth <= triangleWidth;) {
    for (let j = 1; j <= ((triangleWidth - currentRowWidth) / 2); j++) {
        document.write("&nbsp;");
    }
    for (let j = 1; j <= (currentRowWidth); j++) {
        document.write("*");
    }
    currentRowWidth += 2;
    document.writeln("<br>");
}
document.writeln("</div>");

// 3 Заповнений прямокутний трикутник
document.writeln("<div>");
let triangleHeight = 7;
for(let i = 1; i <= triangleHeight; ++i){
    for(let j = 1; j <= i; j++){
        document.write("*");
    }
    document.writeln("<br>");
}
document.writeln("</div>");

// 4 Заповнений ромб
document.writeln("<div>");
currentRowWidth = 1;
let rhombusWidth = 7;
let flagGrowth = true;
for (;currentRowWidth >= 0;) {
    for (let j = 1; j <= ((rhombusWidth - currentRowWidth) / 2); j++) {
        document.write("&nbsp;");
    }
    for (let j = 1; j <= (currentRowWidth); j++) {
        document.write("*");
    }

    if (flagGrowth) {
        currentRowWidth += 2;
    } else {
        currentRowWidth -= 2;
    }
    if (currentRowWidth >= rhombusWidth) {
        flagGrowth = false;
    }
    document.writeln("<br>");
}
document.writeln("</div>");
document.writeln(mainE);



/*
*    ДЗ 2
*/
document.write(footerB);

document.writeln("<b>Завдання:</b> <br> <br>");
document.writeln("1. Створіть масив styles з елементами «Джаз» та «Блюз». <br>");
document.writeln("2. Додайте «Рок-н-рол» до кінця. <br>");
document.writeln("3. Замініть значення всередині на «Класика». Ваш код для пошуку значення всередині повинен працювати для масивів з будь-якою довжиною <br>");
document.writeln("4. Видаліть перший елемент масиву та покажіть його. <br>");
document.writeln("5. Вставте «Реп» та «Реггі» на початок масиву. <br><br><br>");

document.writeln("<b>Рішення:</b> <br>");
const styles = ["Джаз", "Блюз"];
document.writeln("1. [" + styles + "] <br>");

styles.push("Рок-н-рол");
document.writeln("2. [" + styles + "] <br>");

let averageElIdx = parseInt((styles.length / 2).toFixed(0)) - 1;
styles[averageElIdx] = "Класика";
document.writeln(`3. [${styles}], індекс "середнього" елементу = ${averageElIdx} <br>`);

let deletedEl = styles.splice(0, 1);
document.writeln(`4. [${styles}], значення видаленого елементу = "${deletedEl}" <br>`);

styles.unshift("Реп", "Реггі");
document.writeln("5. [" + styles + "] <br>");

document.writeln(footerE);
